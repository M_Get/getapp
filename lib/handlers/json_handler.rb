class JsonHandler
  require 'json'
  require 'yaml'
  def self.show_data(source_file)
    file = JSON.parse(File.open(source_file).read).to_yaml
    file
  end
  def self.output(source_file)
    file = JSON.parse(File.open(source_file).read).to_yaml
    puts file
  end
end
