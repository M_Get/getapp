class YamlHandler
  require 'yaml'
  def self.show_data(source_file)
    file = YAML.load_file(source_file).to_yaml
    file
  end
  def self.output(source_file)
    file = YAML.load_file(source_file).to_yaml
    puts file
  end
end
