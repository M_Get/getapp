class Main
  def self.handle_file(provider, source_file)
    # Calls
    # -----
    # TODO: Here just list the "checker actions" with an if statement
    # to the properly filetype handler, you can find handlers in the
    # handlers folder.

    if is_json?(source_file)
      JsonHandler.show_data(source_file)
    end
    if is_yaml?(source_file)
      YamlHandler.show_data(source_file)
    end
  end

  def self.output(provider, source_file)
    if is_json?(source_file)
      JsonHandler.output(source_file)
    end
    if is_yaml?(source_file)
      YamlHandler.output(source_file)
    end
  end


  # Checkers
  # -------
  # TODO: Here it must add any new file extension that must be checked.
  # Just follow the actions already created as example

  def self.is_json?(source_file)
    File.extname(source_file) == ".json"
  end
  def self.is_yaml?(source_file)
    File.extname(source_file) == ".yaml" || ".yml"
  end
end
