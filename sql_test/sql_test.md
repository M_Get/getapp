## Getapp SQL Test

**Q. Select users whose id is either 3,2 or 4**

    SELECT * FROM test_import.users WHERE id=3 OR id=2 OR id=4;

**Q. Count how many basic and premium listings each active user has**

    select count(*) from test_import.users a INNER JOIN test_import.listings b ON a.id = b.user_id WHERE a.status= 2 AND b.status=2 OR b.status=3;

    This is an "approach" but not solves the question.

**Q. Show the same count as before but only if they have at least ONE premium listing**


**Q. How much revenue has each active vendor made in 2013**

  vendors?

**Q. Insert a new click for listing id 3, at $4.00**

    INSERT INTO test_import.clicks (listing_id, price) VALUES (3, 4.00);

**Q. Show listings that have not received a click in 2013**

    Another Approach

    SELECT clicks.listing_id, clicks.created, listings.id FROM test_import.clicks INNER JOIN test_import.listings ON listings.id = clicks.listing_id WHERE created < '2013/01/01 00:00:00' AND created > '2013/12/31 23:59:59';

**Q. For each year show number of listings clicked and number of vendors who owned these listings**

  vendors?

**Q. Return a comma separated string of listing names for all active vendors**

  vendors?
